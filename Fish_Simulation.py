import sys
import os
import csv

#Variable for calculating New Fish Weight
_weight_new = 0

#constant variables
_tau = 0.08
_alpha = 0.038
_e = 2.718
_beta = 0.6667  

#Initial Fish Weight(grams) and Current Fish Weight  
_weight_ini = _weight_cur = 2000 

#opening csv
try:
	with open('temperature_series.csv', 'r')as _f_csv:
		_fr_ = csv.DictReader(_f_csv)

		for row in _fr_:
			#accessing daily temperature values
			_temp_cur = float(row['Temp']) 

			#calculating fish weight for each day
			_weight_new = (_alpha * _weight_cur**_beta * _e**(_temp_cur * _tau)) + _weight_cur	

			#the calculated weight becomes current weight for next day weight calculation	
			_weight_cur = _weight_new		

	#printing the Initial and Final Fish Weight in Kilogram to terminal		
	print("Initial Fish Weight(Kg):", round((_weight_ini/1000),2))
	print("Final Fish Weight(Kg):", round((_weight_new/1000),2))

#File exception 
except FileNotFoundError as ex:
	print(ex)

#Other exception	
except Exception as ex:
	print(ex)